Как работать в Git, как отправлять лабораторные на Gitlab:
* [Знакомство с системой контроля версий git](git_gitlab_tutorial.md)

Как установить себе Git, QtCreator+CMake, Boost:
* [Установка и настройка среды разработки](cpp_build_setup.md)

Как запускать свои лабораторные в QtCreator:
* [Выполнение лабораторных работ по C++ в среде QtCreator с использованием CMake](how_to_build_cpp_labs_in_qtcreator.md)

